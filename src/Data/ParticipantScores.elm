module Data.ParticipantScores exposing (ParticipantScore, ParticipantScores, decoder)

import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Pipeline exposing (required)


type alias ParticipantScores =
    List ParticipantScore


decoder : Decoder ParticipantScores
decoder =
    Decode.list participantScoreDecoder


type alias ParticipantScore =
    { nickname : String
    , score : Int
    }


participantScoreDecoder : Decoder ParticipantScore
participantScoreDecoder =
    Decode.succeed ParticipantScore
        |> required "nickname" Decode.string
        |> required "score" Decode.int

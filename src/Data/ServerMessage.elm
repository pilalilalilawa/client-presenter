module Data.ServerMessage exposing (ServerMessage(..), decoder)

import Data.Guesses as Guesses exposing (Guesses)
import Data.ParticipantScores as ParticipantScores exposing (ParticipantScores)
import Data.Round as Round exposing (Round)
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Pipeline exposing (required)


type ServerMessage
    = DisplayLobby
    | AskQuestion Round
    | ReportResults ReportResultsData


type alias ReportResultsData =
    { guesses : Guesses
    , participantScores : ParticipantScores
    , moreQuestions : Bool
    }


decoder : Decoder ServerMessage
decoder =
    Decode.andThen
        (\tag ->
            case tag of
                "DisplayLobby" ->
                    Decode.succeed DisplayLobby

                "AskQuestion" ->
                    Decode.map AskQuestion Round.decoder

                "ReportResults" ->
                    Decode.map ReportResults
                        (Decode.succeed ReportResultsData
                            |> required "guesses" Guesses.decoder
                            |> required "participantScores"
                                ParticipantScores.decoder
                            |> required "moreQuestions" Decode.bool
                        )

                _ ->
                    Decode.fail <| "Unknown message type " ++ tag
        )
        (Decode.field "tag" Decode.string)

module Data.AnswerLabel exposing (AnswerLabel(..), toString)


type AnswerLabel
    = A
    | B
    | C
    | D


toString : AnswerLabel -> String
toString answerLabel =
    case answerLabel of
        A ->
            "A"

        B ->
            "B"

        C ->
            "C"

        D ->
            "D"

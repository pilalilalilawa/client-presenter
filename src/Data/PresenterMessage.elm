module Data.PresenterMessage exposing (PresenterMessage(..), encode)

import Json.Encode as Encode


type PresenterMessage
    = RegisterPresenter { key : String }


encode : PresenterMessage -> Encode.Value
encode message =
    case message of
        RegisterPresenter { key } ->
            Encode.object
                [ ( "tag", Encode.string "RegisterPresenter" )
                , ( "key", Encode.string key )
                ]

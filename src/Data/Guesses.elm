module Data.Guesses exposing (Guess, Guesses, decoder)

import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Pipeline exposing (required)


type alias Guesses =
    { a : Guess
    , b : Guess
    , c : Guess
    , d : Guess
    }


decoder : Decoder Guesses
decoder =
    Decode.succeed Guesses
        |> required "a" guessDecoder
        |> required "b" guessDecoder
        |> required "c" guessDecoder
        |> required "d" guessDecoder


type alias Guess =
    { correct : Bool
    , count : Int
    }


guessDecoder : Decoder Guess
guessDecoder =
    Decode.succeed Guess
        |> required "correct" Decode.bool
        |> required "count" Decode.int

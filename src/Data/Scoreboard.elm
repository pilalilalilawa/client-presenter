module Data.Scoreboard exposing (Scoreboard, ScoreboardEntry, fromParticipantScores)

import Data.ParticipantScores exposing (ParticipantScores)


type alias Scoreboard =
    List ScoreboardEntry


type alias ScoreboardEntry =
    { place : Maybe Int
    , nickname : String
    , score : Int
    }


fromParticipantScores : ParticipantScores -> Scoreboard
fromParticipantScores participantScores =
    .scoreboard <|
        List.foldl
            (\{ nickname, score } { place, previousScore, scoreboard } ->
                { place = place + 1
                , previousScore = Just score
                , scoreboard =
                    scoreboard
                        ++ [ { place =
                                if previousScore == Just score then
                                    Nothing

                                else
                                    Just place
                             , nickname = nickname
                             , score = score
                             }
                           ]
                }
            )
            { place = 1
            , previousScore = Nothing
            , scoreboard = []
            }
            (List.sortBy (\{ nickname, score } -> ( -score, nickname )) participantScores)

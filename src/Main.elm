port module Main exposing (main)

import Browser
import Browser.Navigation as Navigation
import Data.AnswerLabel as AnswerLabel exposing (AnswerLabel(..))
import Data.Guesses exposing (Guess, Guesses)
import Data.PresenterMessage as PresenterMessage exposing (PresenterMessage(..))
import Data.Round exposing (Round)
import Data.Route as Route
import Data.Scoreboard as Scoreboard exposing (Scoreboard)
import Data.ServerMessage as ServerMessage exposing (ServerMessage(..))
import Element
    exposing
        ( Column
        , Element
        , alignBottom
        , alignRight
        , alignTop
        , centerX
        , centerY
        , column
        , el
        , fill
        , height
        , html
        , htmlAttribute
        , inFront
        , layout
        , link
        , newTabLink
        , none
        , padding
        , paragraph
        , px
        , rgb
        , row
        , shrink
        , spaceEvenly
        , spacing
        , table
        , text
        , width
        )
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Input as Input exposing (button)
import Element.Region as Region
import Html exposing (form)
import Html.Attributes as Attributes exposing (id, style, type_, value)
import Html.Events exposing (onClick, onInput, onSubmit)
import Json.Decode as Decode exposing (decodeString, decodeValue)
import Json.Encode as Encode exposing (encode)
import Octicons
import Platform.Cmd exposing (Cmd)
import Platform.Sub exposing (Sub)
import QRCode
import Url exposing (Url)
import Url.Builder


main : Program Flags Model Msg
main =
    Browser.application
        { init = init
        , onUrlChange = UrlChange
        , onUrlRequest = UrlRequest
        , view = view
        , update = update
        , subscriptions = subscriptions
        }


type alias Flags =
    { participantUrl : String
    , brand : String
    }


type alias Model =
    { state : State
    , key : String
    , navigationKey : Navigation.Key
    , participantUrl : String
    , brand : String
    }


type State
    = Start
    | Connecting
    | Lobby
    | QuestionAsked Round
    | ResultDisplay
        { guesses : Guesses
        , scoreboard : Scoreboard
        , moreQuestions : Bool
        }
    | PageNotFound


init : Flags -> Url -> Navigation.Key -> ( Model, Cmd Msg )
init { participantUrl, brand } url navigationKey =
    let
        ( state, key, cmd ) =
            navigate url
    in
    ( { state = state
      , key = key
      , navigationKey = navigationKey
      , participantUrl = participantUrl
      , brand = brand
      }
    , cmd
    )


navigate : Url -> ( State, String, Cmd Msg )
navigate url =
    case Route.fromUrl url of
        Just route ->
            case route of
                Route.Root ->
                    ( Start, "", Cmd.none )

                Route.Quiz key ->
                    ( Connecting
                    , key
                    , sendMessage <| RegisterPresenter { key = key }
                    )

        Nothing ->
            ( PageNotFound, "", Cmd.none )


type Msg
    = UrlChange Url
    | UrlRequest Browser.UrlRequest
    | ReceiveData String
    | KeyChanged String
    | StartPageSubmit


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case ( model.state, msg ) of
        ( _, UrlChange url ) ->
            let
                ( newState, newKey, cmd ) =
                    navigate url
            in
            ( { model | state = newState, key = newKey }, cmd )

        ( _, UrlRequest request ) ->
            case request of
                Browser.Internal url ->
                    ( model
                    , Navigation.pushUrl
                        model.navigationKey
                        (Url.toString url)
                    )

                Browser.External href ->
                    ( model, Navigation.load href )

        ( _, ReceiveData data ) ->
            case decodeString ServerMessage.decoder data of
                Ok message ->
                    case message of
                        DisplayLobby ->
                            ( { model | state = Lobby }, Cmd.none )

                        AskQuestion round ->
                            ( { model | state = QuestionAsked round }
                            , Cmd.none
                            )

                        ReportResults { guesses, participantScores, moreQuestions } ->
                            ( { model
                                | state =
                                    ResultDisplay
                                        { guesses = guesses
                                        , scoreboard =
                                            Scoreboard.fromParticipantScores
                                                participantScores
                                        , moreQuestions = moreQuestions
                                        }
                              }
                            , Cmd.none
                            )

                Err error ->
                    ( model, Cmd.none )

        ( Start, KeyChanged value ) ->
            ( { model | key = value }
            , Cmd.none
            )

        ( Start, StartPageSubmit ) ->
            ( model
            , case model.key of
                "" ->
                    Cmd.none

                _ ->
                    Navigation.pushUrl
                        model.navigationKey
                        (Route.build <| Route.Quiz model.key)
            )

        _ ->
            ( model, Cmd.none )


subscriptions : Model -> Sub Msg
subscriptions _ =
    receiveData ReceiveData


view : Model -> Browser.Document Msg
view model =
    { title = "Pilalilalilawa!"
    , body = [ layout [] <| ui model ]
    }


ui : Model -> Element Msg
ui model =
    column [ width fill, height fill ]
        [ row
            [ Region.navigation
            , width fill
            , height (px 64)
            , spaceEvenly
            , padding 16
            ]
            [ text model.brand
            , row []
                [ text "powered by "
                , link [ Font.underline ]
                    { url = "https://pilalilalilawa.dev/"
                    , label = text "free software"
                    }
                , text " "
                , html <| Octicons.heart Octicons.defaultOptions
                ]
            ]
        , el [ Region.mainContent, width fill, height fill ] <|
            contentView model
        ]


contentView : Model -> Element Msg
contentView model =
    case model.state of
        Start ->
            el [ centerX, centerY ] <|
                column [ spacing 8 ]
                    [ html <|
                        form
                            [ onSubmit StartPageSubmit
                            , id "start-page-form"
                            ]
                            []
                    , Input.text
                        [ htmlAttribute <| Attributes.form "start-page-form"
                        ]
                        { onChange = KeyChanged
                        , text = model.key
                        , placeholder = Nothing
                        , label = Input.labelAbove [] (text "Key")
                        }
                    , button
                        [ htmlAttribute <| Attributes.form "start-page-form"
                        , width fill
                        , padding 8
                        , Border.width 1
                        ]
                        { onPress = Just StartPageSubmit
                        , label = el [ centerX ] <| text "Connect"
                        }
                    ]

        Connecting ->
            statusMessage "Connecting..."

        Lobby ->
            let
                url =
                    Url.Builder.crossOrigin
                        model.participantUrl
                        [ "q", model.key ]
                        []
            in
            el [ centerX, centerY ] <|
                column [ spacing 16 ]
                    [ el [ centerX ] <| text "Go to"
                    , newTabLink [ centerX ]
                        { url = model.participantUrl
                        , label =
                            el [ Font.size 40 ] <|
                                text model.participantUrl
                        }
                    , el [ centerX ] <| text "and enter the key code"
                    , el [ centerX, Font.size 48 ] <| text model.key
                    , QRCode.encode url
                        |> Result.map (el [centerX] << html << QRCode.toSvg)
                        |> Result.withDefault none
                    ]

        QuestionAsked round ->
            el [ width fill, height fill, padding 8 ] <|
                column [ width fill, height fill, spacing 8 ]
                    [ el [ width fill, height fill, Border.width 1 ] <|
                        paragraph
                            [ centerX
                            , centerY
                            , padding 16
                            , htmlAttribute <| style "text-align" "center"
                            ]
                            [ text round.question ]
                    , column [ width fill, height fill, spacing 8 ]
                        [ row [ width fill, height fill, spacing 8 ]
                            [ answerView round.answers.a
                            , answerView round.answers.b
                            ]
                        , row [ width fill, height fill, spacing 8 ]
                            [ answerView round.answers.c
                            , answerView round.answers.d
                            ]
                        ]
                    ]

        ResultDisplay { guesses, scoreboard, moreQuestions } ->
            row [ width fill, height fill ]
                [ el [ width fill, height fill ] <|
                    el [ centerX, centerY ] <|
                        el [] <|
                            guessesView guesses
                , el [ width fill, height fill ] <|
                    el [ centerX, centerY ] <|
                        column [ spacing 32 ]
                            [ el [ centerX ] <| scoreboardView scoreboard
                            , if moreQuestions then
                                none

                              else
                                column [ spacing 4 ]
                                    [ el [ centerX ] <|
                                        text "The quiz is over."
                                    , el [ centerX, Font.size 24 ] <|
                                        text "Thanks for participating!"
                                    ]
                            ]
                ]

        PageNotFound ->
            statusMessage "Page not found."


guessesView : Guesses -> Element Msg
guessesView guesses =
    let
        guessesList =
            [ ( A, guesses.a )
            , ( B, guesses.b )
            , ( C, guesses.c )
            , ( D, guesses.d )
            ]

        total =
            List.sum <| List.map (.count << Tuple.second) guessesList
    in
    row [ spacing 16 ] <| List.map (guessesColumn total) guessesList


guessesColumn : Int -> ( AnswerLabel, Guess ) -> Element Msg
guessesColumn total ( answerLabel, { correct, count } ) =
    column []
        [ el [ width (px 64), height (px 256) ] <|
            el
                [ width (px 64)
                , (height << px << round)
                    (toFloat 256 / toFloat total * toFloat count)
                , Background.color (rgb 0 0 0)
                , alignBottom
                ]
                none
        , el [ width (px 64), height (px 64) ] <|
            el [ centerX, centerY ] <|
                text (String.fromInt count)
        , el
            [ width (px 64)
            , height (px 64)
            , Background.color (rgb 0.9 0.9 0.9)
            , inFront <|
                el
                    [ width (px 64)
                    , height (px 64)
                    ]
                <|
                    el [ centerX, centerY, Font.size 32 ] <|
                        text (AnswerLabel.toString answerLabel)
            ]
          <|
            el [ centerX, centerY ] <|
                html <|
                    let
                        options =
                            Octicons.defaultOptions
                                |> Octicons.size 64
                                |> Octicons.color "rgb(75%, 75%, 75%)"
                    in
                    if correct then
                        Octicons.check options

                    else
                        Octicons.x options
        ]


scoreboardView : Scoreboard -> Element Msg
scoreboardView scoreboard =
    table []
        { data = scoreboard
        , columns =
            [ tableColumn
                ( "#"
                , (maybe none <| el [ alignRight ] << text << String.fromInt)
                    << .place
                )
            , tableColumn ( "Nickname", text << .nickname )
            , tableColumn
                ( "Score"
                , el [ alignRight ] << text << String.fromInt << .score
                )
            ]
        }


maybe : b -> (a -> b) -> Maybe a -> b
maybe default fn =
    Maybe.withDefault default << Maybe.map fn


tableColumn : ( String, record -> Element msg ) -> Column record msg
tableColumn ( label, fn ) =
    { header = el [ padding 8 ] <| text label
    , width = shrink
    , view = el [ padding 8 ] << fn
    }


answerView : String -> Element Msg
answerView answerText =
    el [ width fill, height fill, Border.width 1 ] <|
        paragraph
            [ centerX
            , centerY
            , padding 16
            , htmlAttribute <| style "text-align" "center"
            ]
            [ text answerText ]


statusMessage : String -> Element Msg
statusMessage message =
    el [ centerX, centerY ] <| text message


sendMessage : PresenterMessage -> Cmd Msg
sendMessage =
    sendData << encode 0 << PresenterMessage.encode


port sendData : String -> Cmd msg


port receiveData : (String -> msg) -> Sub msg
